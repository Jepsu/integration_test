import React, { useState } from "react";

function HexToRgbConverter() {
  const [hex, setHex] = useState("");
  const [rgb, setRgb] = useState("");

  const handleHexConvert = async () => {
    const hexValue = hex;

    try {
      const response = await fetch('http://172.30.134.134/hex-to-rgb', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ hex: hexValue }),
      });

      if (response.ok) {
        const data = await response.json();
        const rgbString = `RGB Result: ${data.rgb.r}, ${data.rgb.g}, ${data.rgb.b}`;

        setRgb(rgbString);
      } else {
        console.error('Failed to convert hex to RGB');
      }
    } catch (error) {
      console.error('Error while converting hex to RGB', error);
    }
  };

  return (
    <div className="input-hex-container">
      <h2>Hex to RGB Converter</h2>
      <form>
        <input
          type="text"
          placeholder="Enter Hex Color"
          value={hex}
          onChange={(e) => setHex(e.target.value)}
        />
        <button type="button" onClick={handleHexConvert}>
          Convert
        </button>
      </form>
      
      {rgb && (
      <div className="rgb-value-container">
        <p>RGB Value: {rgb}</p>
      </div>
      )}
    </div>
  );
}

export default HexToRgbConverter;