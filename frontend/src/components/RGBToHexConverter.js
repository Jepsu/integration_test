import React, { useState} from 'react';

function RGBToHexConverter() {
  const [red, setRed] = useState('');
  const [green, setGreen] = useState('');
  const [blue, setBlue] = useState('');
  const [hexValue, setHexValue] = useState('');
  const [error, setError] = useState('');
  const [previousConversions, setPreviousConversions] = useState([]);

  const handleConvert = async () => {
    const redValue = parseInt(red, 10);
    const greenValue = parseInt(green, 10);
    const blueValue = parseInt(blue, 10);

    if (isNaN(redValue) || isNaN(greenValue) || isNaN(blueValue)) {
      setError('Invalid RGB values. Please enter integers between 0 and 255.');
      return;
    }

    try {
      const response = await fetch('http://172.30.134.134/rgb-to-hex', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ red: redValue, green: greenValue, blue: blueValue }),
      });

      if (response.ok) {
        const data = await response.json();
        const hex = data.hex;

        setHexValue(hex);
        setError('');

        // Get the current date and time
        const currentDate = new Date();
        const formattedDate = currentDate.toLocaleString();

        // Add the current conversion to the array of previous conversions
        const newConversion = {
          red: redValue,
          green: greenValue,
          blue: blueValue,
          hex: hex,
          date: formattedDate,
        };
        setPreviousConversions([newConversion, ...previousConversions]);
      } else {
        setError('Error converting RGB to HEX');
        setHexValue('');
      }
    } catch (error) {
      console.error('Error making the POST request:', error);
    }
  };

  return (
    <div>
      <h2>RGB to HEX Converter</h2>
      <form>
        <input
          type="number"
          placeholder="Red"
          value={red}
          onChange={(e) => setRed(e.target.value)}
        />
        <input
          type="number"
          placeholder="Green"
          value={green}
          onChange={(e) => setGreen(e.target.value)}
        />
        <input
          type="number"
          placeholder="Blue"
          value={blue}
          onChange={(e) => setBlue(e.target.value)}
        />
        <button type="button" onClick={handleConvert}>
          Convert
        </button>
      </form>

      {hexValue && (
        <div className="hex-value-container">
          <p>HEX Value: {hexValue}</p>
        </div>
      )}

      {error && (
        <div>
          <p>Error: {error}</p>
        </div>
      )}

      <button onClick={() => setPreviousConversions([])}>Clear Previous Conversions</button>

      {previousConversions.length > 0 && (
        <div>
          <h3>Previous Conversions</h3>
          <ul style={{ maxHeight: '100px', overflow: 'auto' }}>
            {previousConversions.map((conversion, index) => (
              <li key={index}>
                RGB: ({conversion.date}{conversion.red}, {conversion.green}, {conversion.blue}) - HEX: {conversion.hex}
              </li>
            ))}
          </ul>
        </div>
      )}
    </div>
  );
}

export default RGBToHexConverter;