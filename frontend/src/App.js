// src/App.js
import React from 'react';
import './App.css';
import RGBToHexConverter from './components/RGBToHexConverter';
import HexToRGBConverter from './components/HexToRGBConverter';




function App() {
  return (
    <div className="App">
      <RGBToHexConverter />
      <HexToRGBConverter />      
    </div>
  );
}

export default App;