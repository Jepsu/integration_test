const bodyParser = require('body-parser');
const express = require('express');
const { request } = require('express')


const app = express();
const port = 4001;
const converter = require("./converter")

app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post("/rgb-to-hex", (req, res) => {
  const { red, green, blue } = req.body;
  console.log(red, green, blue);

  try {
    const hex = converter.ConvertRGBtoHex(red, green, blue);
    res.json({ "hex": hex });
    console.log(hex);
    //const hex = "#FF0000";
    //res.json({ "hex": hex });
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});


app.post("/hex-to-rgb", (req, res) => {
  const { hex } = req.body; // Extract the hex value from the request body

  if (hex) {
    const rgb = converter.hexToRgb(hex);
    res.json({ rgb });
  } else {
    res.status(400).json({ error: 'Please provide a valid HEX value in the request body.' });
  }
});

if (process.env.NODE_ENV === 'test'){
  module.exports = app;

} else {
  app.listen(port, () => console.log(`Server : Localhost:${port}`));
}



