// TDD - unit testing

const expect = require("chai").expect;
const converter = require("../src/converter.js");

describe("Color Code Converter", () => {
    describe("RGB to Hex conversion", () =>{
        it("converts to the basic colors", () => {
            const redHex = converter.ConvertRGBtoHex(255,0,0);
            const greenHex = converter.ConvertRGBtoHex(0,255,0);
            const blueHex = converter.ConvertRGBtoHex(0,0,255);

            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(blueHex).to.equal("0000ff");
        
        });
    });
});